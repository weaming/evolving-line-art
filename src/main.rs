/// https://gkbrk.com/2018/01/evolving-line-art/

extern crate image;
extern crate imageproc;
extern crate rand;

use std::fs::File;
use std::path::Path;
use rand::Rng;
use std::env;

use image::{DynamicImage, GenericImage, Pixel};

fn image_diff(img1: &DynamicImage, img2: &DynamicImage) -> f64 {
    imageproc::stats::root_mean_squared_error(img1, img2)
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut size: i32 = 5;
    if args.len() > 1 {
        size = args[1].parse::<i32>().unwrap();
    }

    let target = image::open("target.png").expect("Cannot load target.png");

    let mut img1 = DynamicImage::new_rgb8(target.width(), target.height());
    let mut img2 = DynamicImage::new_rgb8(target.width(), target.height());

    let mut colours = Vec::new();

    for pixel in target.pixels() {
        let rgba = pixel.2.to_rgba();

        if !colours.contains(&rgba) {
            colours.push(rgba);
        }
    }

    let mut rng = rand::thread_rng();

    let mut i = 0;
    loop {
        let pos:   (i32, i32) = (rng.gen_range(0, target.width() as i32), rng.gen_range(0, target.height() as i32));
        let colour = rng.choose(&colours).unwrap();

        imageproc::drawing::draw_filled_circle_mut(&mut img1, pos, size, *colour);

        if image_diff(&target, &img1) < image_diff(&target, &img2) {
            &img2.copy_from(&img1, 0, 0);
        } else {
            &img1.copy_from(&img2, 0, 0);
        }

        if i % 100 == 0 {
            println!("{}", i);
            img2.save(&mut File::create(&Path::new("output.png")).unwrap(), image::PNG);
        }

        i += 1;
    }
}
